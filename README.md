# emilie-is-older

**Network Id:** `18677129469693`
First and foremost, make sure you [install geth](https://ethereum.org/cli)

First and foremost, make sure you [install geth](https://ethereum.org/cli)

## Useful Commands
You should probably change the `--identity` flag to be your own name.

### Setup
```
cd ~
git clone git@bitbucket.org:mentsquad/emilie-is-older.git
export ETH_GEN="$HOME/emilie-is-older/private-genesis.json"
```

*NOTE: to read the logs, enter `tail -f /tmp/geth.log` in a separate terminal*

### Initialize blockchain (performs sync)
```
geth --identity "TyronesTestNetNode" --nodiscover --networkid 18677129469693 init $ETH_GEN 2>> /tmp/geth.log
```

### Run console
```
geth --identity "TyronesTestNetNode" --nodiscover --networkid 18677129469693 console 2>> /tmp/geth.log
```

## Attaching to our private network
You just need to add one of the existing nodes as a peer. Note the `TYRONES_IP_GOES_HERE`--you'll need to ask Tyrone for his IP address to replace this part.
```
> admin.addPeer("enode://aae81b02ebb626f4bd3d1382e0bf38e000d6c8faca48184f57bab989ba4f8fee5cb192e42adab875fa80fc5f2936f95ff7c03676cd2676f728bf19a697d61197@TYRONES_IP_GOES_HERE:30303?discport=0")
true
> admin.peers
1
```
*NOTE: Replace the `[::]` with the IP address of the node you'd like to peer with. This should be the LAN IP for private networks.*

